<?php
/**
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */
define('HTTP_HOST', $_SERVER["HTTP_HOST"]);
define('DEV_1', 'nombre.dominio');
define('DEV_2', 'landing.yaru.local');
define('DEV_3', 'nombre.dominio');
define('DEV_DESARROLLO', 'nombre.dominio');
define('PROD_DOMAIN', 'fundacionwwbcolombia.org/yaru-camino-integral-emprendedoras');
define('DEV_LOCAL_VIEW', 'nombre.dominio');


// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //

switch (HTTP_HOST) {
    case DEV_1:
        /** El nombre de tu base de datos de WordPress */
        define('WP_HOME','http://nombre.develop/');
        define('WP_SITEURL','http://nombre.develop/');
        /** El nombre de tu base de datos de WordPress */
        define('DB_NAME', '');
        /** Tu nombre de usuario de MySQL */
        define('DB_USER', '');
        /** Tu contraseña de MySQL */
        if ( substr(PHP_OS, 0, 3) === 'WIN' ) {
            define('DB_PASSWORD', '');
        }
        else {
            define('DB_PASSWORD', 'root');
        }
        /** Host de MySQL (es muy probable que no necesites cambiarlo) */
        define('DB_HOST', 'localhost');
        break;

    case DEV_2:
        /** El nombre de tu base de datos de WordPress */
        define('WP_HOME','http://landing.yaru.local');
        define('WP_SITEURL','http://landing.yaru.local');
        /** El nombre de tu base de datos de WordPress */
        define('DB_NAME', 'dev_yaru_local');
        /** Tu nombre de usuario de MySQL */
        define('DB_USER', 'root');
        /** Tu contraseña de MySQL */
        if ( substr(PHP_OS, 0, 3) === 'WIN' ) {
            define('DB_PASSWORD', '');
        }
        else {
            define('DB_PASSWORD', 'root');
        }
        /** Host de MySQL (es muy probable que no necesites cambiarlo) */
        define('DB_HOST', 'localhost');
        break;

    case DEV_3:
        /** El nombre de tu base de datos de WordPress */
        define('WP_HOME','http://nombre.develop/');
        define('WP_SITEURL','http://nombre.develop/');
        /** El nombre de tu base de datos de WordPress */
        define('DB_NAME', '');
        /** Tu nombre de usuario de MySQL */
        define('DB_USER', 'root');
        /** Tu contraseña de MySQL */
        if ( substr(PHP_OS, 0, 3) === 'WIN' ) {
            define('DB_PASSWORD', '');
        }
        else {
            define('DB_PASSWORD', 'root');
        }
        /** Host de MySQL (es muy probable que no necesites cambiarlo) */
        define('DB_HOST', 'localhost');
        break;

    case DEV_DESARROLLO:
        /*SERVIDOR DE VECTORIAL*/
        define('WP_HOME','http://nombre.develop/');
        define('WP_SITEURL','http://nombre.develop/');
        /** El nombre de tu base de datos de WordPress */
        define('DB_NAME', '');
        /** Tu nombre de usuario de MySQL */
        define('DB_USER', 'root');
        /** Tu contraseña de MySQL */
        define('DB_PASSWORD', '');
        /** Host de MySQL (es muy probable que no necesites cambiarlo) */
        define('DB_HOST', '127.0.0.1');
        break;

    case DEV_LOCAL_VIEW:
        /** El nombre de tu base de datos de WordPress */
        define('WP_HOME','');
        define('WP_SITEURL','');

        define('DB_NAME', '');
        /** Tu nombre de usuario de MySQL */
        define('DB_USER', 'root');
        /** Tu contraseña de MySQL */
        if ( substr(PHP_OS, 0, 3) === 'WIN' ) {
            define('DB_PASSWORD', '');
        }
        else {
            define('DB_PASSWORD', 'root');
        }
        /** Host de MySQL (es muy probable que no necesites cambiarlo) */
        define('DB_HOST', 'localhost');
        break;

    case PROD_DOMAIN:
        /** El nombre de tu base de datos de WordPress */
        define('WP_HOME','http://fundacionwwbcolombia.org/yaru-camino-integral-emprendedoras');
        define('WP_SITEURL','http://fundacionwwbcolombia.org/yaru-camino-integral-emprendedoras');
        /** El nombre de tu base de datos de WordPress */
        define('DB_NAME', 'FundacionWWBYaru');
        /** Tu nombre de usuario de MySQL */
        define('DB_USER', 'fundacion_wwb');
        /** Tu contraseña de MySQL */
        define('DB_PASSWORD', 'fuN$d4c1$0n$w3B');
        /** Host de MySQL (es muy probable que no necesites cambiarlo) */
        define('DB_HOST', 'dbproduccion1.cartbphlockr.us-west-2.rds.amazonaws.com');
        break;

    default:
        /** El nombre de tu base de datos de WordPress */
        define('DB_NAME', 'FundacionWWBYaru');
        /** Tu nombre de usuario de MySQL */
        define('DB_USER', 'fundacion_wwb');
        /** Tu contraseña de MySQL */
        define('DB_PASSWORD', 'fuN$d4c1$0n$w3B');
        /** Host de MySQL (es muy probable que no necesites cambiarlo) */
        define('DB_HOST', 'dbproduccion1.cartbphlockr.us-west-2.rds.amazonaws.com');
        break;
}
/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/*Eliminar saltos de linea en formularios Contact FORM 7*/
define('WPCF7_AUTOP', false );

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '|]&1dn97nJdS+6,~TQg+Qd%n&vK:x+s:$^t- lMu73m2GQ18)7G?1l6^wj/-6TPg');
define('SECURE_AUTH_KEY',  '0r-C|vMJH|HI%K~wE^=5?kb]v>jlGE{#zpIv9Se0Ta-Atk9wHapP2fq[Q0xf~+Ke');
define('LOGGED_IN_KEY',    '35|8b+JSg5kT3fDAOxAkhhc ])8T?]ts-jiI/x:14.vN]!>vowH<`quZaE.V6Mh|');
define('NONCE_KEY',        'DN[SH&B+|I$5|e~t@8N)_%tuUxzfGIuDu&Gn+f[TJf#.e5ikNY9aQt+F)R6DK&cw');
define('AUTH_SALT',        'E P=PY~jRuCO4bk!F~^/iW+zWm6nfxu+2-tDc-R=__xhSEF;GTrP3~u>eE#X)vJc');
define('SECURE_AUTH_SALT', 'Hjbbg6@lHQq-/&8)lR+k7K4_<zZ3U^kx2P}~Cbg4+,ZYhBZgZ&p)|rlm3-}6#1~!');
define('LOGGED_IN_SALT',   'E:0F.-Rv<UK3H[:tZ]s*b&/e|fIY%Lp0I$mFbxWLYPov N~T;5GLmW^/uE6&L~uG');
define('NONCE_SALT',       '!nk+;,4(Whz2~,r-ZS7G_]B9OxWW]J?uT} 5eZ{.vM:yrO/dq?ZtkA>Q(+UUJH)P');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'yrwp_';

/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

