<?php
/* Template Name: Home */
get_header();
wp_head();
?>
    <div id="fullpage">

        <!-- SECCIONES PAGINA-->

        <!-- INICIO SECCION 1-->
        <div class="section cabezote-yaru fp-auto-height-responsive" id="section0">
            <div class="row">
                <div class="col-md-8 col-lg-9">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 logo-yaru">
                            <img class="img-responsive" src="<?php bloginfo('template_url') ?>/img/logo-yaru-cabezote.png" alt="" width="100%">
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7 title-cabezote">
                            <p class="text-left">Transforma tu negocio </br>en algo maravilloso,</br></p>
                            <span>¡Capacítate!</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-9 text-center content-arrow btn1 " style="text-align: center;">
                <a href="#que-es-yaru"><i class="fa fa-angle-down " aria-hidden="true"></i></br><span>Haz Clic</br>para ver más</span></a>
            </div>
        </div>
        <!-- FIN SECCION 1-->

        <div class="section que-es-yaru" id="section1">
            <div class="row">
                <div class="col-md-8 col-lg-9">
                    <div class="col-md-12 col-lg-5 title-content">
                        <h2>¿Qué <span>es Yarú:</span></h2>
                        <p >Camino Integral para Emprendedoras?</p>
                    </div>
                    <div class="col-md-12 col-lg-7 content">
                        <p>Es el programa de capacitación integral, no formal y presencial de la Fundación WWB Colombia dirigido a emprendedoras de escasos recursos. Su propósito es que, en cuatro niveles de formación, las emprendedoras avancen <span>en sus negocios, en sus proyectos de vida personal y en sus relaciones familiares.</span></p>
                    </div>
                </div>
                <!-- <div class="col-md-9 bg-que-es">
                    <img src="img/que-es-yaru.png" alt="" width="100%">
                </div>-->
            </div>
            <div class="col-xs-12 col-sm-12  col-md-8 col-lg-9 text-center content-arrow">
                <a href="#como-lo-hacemos"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
            </div>
        </div>


        <div class="section como-lo-hacemos" id="section2">

            <div class="row">
                <div class="col-md-8 col-lg-9">
                    <div class="col-md-12 title-content">
                        <h2>¿Cómo lo <span class="">hacemos?</span></h2>
                    </div>
                    <div class="col-md-12 content">
                        <p>Con Yarú: Camino Integral para Emprendedoras, te capacitamos a través de tutorías académicas y de asesorías
                            empresariales, a identificar los objetivos de tu  negocio, para <span>impulsarlo, fortalecerlo y proyectarlo en el tiempo.</span></p>
                        </br>
                        <p>Tenemos una <span>metodología única basada en el juego y la pedagogía para adultos.</span> En ‘Iniciativa Emprendedora’,
                            el Nivel 1 de Yarú: Camino Integral para empremdedoras aprenderás sobre:</p>

                    </div>
                    <div class="col-md-12 col-lg-10 col-lg-offset-1 content">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-3 img-como-lo-hacemos">
                                <img class="img-responsive" src="<?php bloginfo('template_url') ?>/img/emprendimiento.png" alt="" width="100%">
                                <p>Emprendimiento</p>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3 img-como-lo-hacemos">
                                <img class="img-responsive" src="<?php bloginfo('template_url') ?>/img/educacion-financiera.png" alt="" width="100%">
                                <p>Educación</br>financiera</p>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3 img-como-lo-hacemos">
                                <img class="img-responsive" src="<?php bloginfo('template_url') ?>/img/alfabetizacion-digital.png" alt="" width="100%">
                                <p>Alfabetización</br>digital</p>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3 img-como-lo-hacemos">
                                <img class="img-responsive" src="<?php bloginfo('template_url') ?>/img/liderazgo.png" alt="" width="100%">
                                <p>Liderazgo</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12  col-md-8 col-lg-9 text-center content-arrow">
                <a href="#sesiones"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
            </div>
        </div>
        <div class="section sesiones" id="section3">
            <div class="row">
                <div class="col-md-8 col-lg-9">
                    <div class="row">
                        <div class="col-md-4 content text-center">
                            <img class="img-responsive img-logo-iniciativa" src="<?php bloginfo('template_url') ?>/img/logo-iniciativa-emprendedora-nivel1.png" alt="" width="100%"></br>
                            <div class="col-md-12 costo-session text-center">Costo por sesión: $18.000</div>
                        </div>
                        <div class="col-md-8 content text-center">
                            <p>SESIONES</p>
                            <img class="img-responsive img-sesiones" src="<?php bloginfo('template_url') ?>/img/sesiones.png" alt="" width="100%">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-9 text-center content-arrow" style="z-index: 99999;">
                <a href="#programa-fortaleser"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
            </div>
        </div>
        <div class="section programa-fortaleser" id="section4">

            <div class="row">
                <div class="col-md-8 col-lg-9 content">
                    <div class="row">
                        <div class="col-md-7 col-lg-7 ">
                            <p>Además, contarás con el Programa FortaleSer el cual consta de <span>actividades transversales a la ruta de formación como eventos, charlas y kits para la familia</span> con las que buscamos brindarte a ti y a tu seres queridos información relevante sobre:</p>
                        </div>
                        <div class="col-md-5 col-lg-5">
                            <ul>
                                <li>- Formalización de empresas</li>
                                <li>- Mercadeo digital</li>
                                <li>- Salud sexual y reproductiva</li>
                                <li>- Realización de eventos anuales</li>
                                <li>- Prevención de violencia y adicciones</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-9 img-section">
                    <div class="col-md-4 logo-fortalecer"><img src="<?php bloginfo('template_url') ?>/img/logo-programa-fortalecer.png" alt="" width="100%"></div>
                    <!-- <div class="col-md-8 grupo-mujeres"><img src="<?php //bloginfo('template_url') ?>/img/grupo-mujeres-programafortalecer.png" alt="" width="100%"></div> -->
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-9 text-center content-arrow" style="z-index: 99999;">
                <a href="#elige-yaru"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
            </div>
        </div>


        <div class="section elige-yaru" id="section5">
            <div class="row">
                <div class="col-md-8 col-lg-9">
                    <div class="col-md-12 title-content">
                        <h2><span>¡Elige Yarú:</span></h2>
                        <p>Camino Integral para Emprendedoras!</p>
                    </div>
                    <div class="col-md-12 content">
                        <p>Así como tú, ellas tomaron la decisión de emprender y capacitarse para alcanzar sus objetivos. <span>Haz clic y conoce sus historias.</span></p>
                    </div>
                    <div class="col-md-12 content-emprendedoras">
                        <div class="col-md-6 emprendedoras">
                            <a href="https://www.fundacionwwbcolombia.org/zucchero-un-emprendimiento-del-cielo-tu-boca/" target="_blank"><div class="box">
                                <img class="img-responsive" src="<?php bloginfo('template_url') ?>/img/jeimmy-gaviria.png" alt="">
                                <p>“Las capacitaciones que nos brindan son muy buenas. No es por ‘echarles flores’, pero <span>como docente me di cuenta de lo bien planeado que tienen cada nivel.”</span></p>
                                <span class="nombre-emp">Jeimmy Gaviria – Zucchero</span>
                            </div></a>
                        </div>
                        <div class="col-md-6 emprendedoras">
                            <a href="https://www.fundacionwwbcolombia.org/un-emprendimiento-tan-dulce-y-colombiano-como-la-aguapanela/" target="_blank"><div class="box">
                                <img class="img-responsive" src="<?php bloginfo('template_url') ?>/img/diana-ortega.png" alt="">
                                <p>“En la Fundación WWB Colombia no solo aprendí cosas nuevas para mi negocio, sino que <span>construí una gran red de contactos con emprendedoras como yo.”</span></p>
                                <span class="nombre-emp">Diana Ortega – Aguapanela Dulce Imaginación</span>
                            </div></a>
                        </div>
                        <div class="col-md-6 emprendedoras">
                            <a href="https://www.fundacionwwbcolombia.org/lila-el-color-de-la-esperanza-para-mujeres-con-vih-en-cali/" target="_blank"><div class="box">
                                <img class="img-responsive" src="<?php bloginfo('template_url') ?>/img/yaneth-valencia.png" alt="">
                                <p>“Lo que empiezo, lo termino; y aunque en muchas ocasiones tuve crisis, logré culminar las capacitaciones en la Fundación y <span>aprendí a organizar mejor mi negocio.”</span></p>
                                <span class="nombre-emp">Yaneth Valencia – Asociación Lila Mujer</span>
                            </div></a>
                        </div>
                        <div class="col-md-6 emprendedoras">
                            <a href="https://www.fundacionwwbcolombia.org/casa-dulce-cali-una-pasteleria-que-brinda-capacitacion-culinaria/" target="_blank"><div class="box">
                                <img class="img-responsive" src="<?php bloginfo('template_url') ?>/img/diana-davila.png" alt="">
                                <p><span>“Las enseñanzas de la Fundación WWB Colombia son tan útiles y de calidad,</span> que siento que en la universidad no enseñan de una manera práctica.”</p>
                                <span class="nombre-emp">Diana Dávila – Casa Dulce Cali</span>
                            </div></a>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-9 text-center content-arrow">
                <a href="#fundacionwwb"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
            </div>

        </div>

        <div class="section fundacionwwb" id="section6">

            <div class="row">
                <div class="col-md-8 col-lg-9 content">
                    <h2>Conoce a la <span>Fundación WWB Colombia</span>
                    </h2>
                    </br>
                    <p><span>La Fundación WWB Colombia</span> lleva más de 30 años apoyando a la mujer microempresaria, llegándose a posicionar como una institución microfinanciera líder en el país. En el año 2011 creó el <span>Banco WWB S.A.</span>, hoy <span>Banco W</span>, para continuar su enfoque social como Fundación, trabajando en proyectos que contribuyen al reconocimiento y proyección de las mujeres, a través de la capacitación, el liderazgo y el empoderamiento, con el fin de mejorar su calidad de vida y la de sus familias. </p>
                    </br>
                    <p>Desde el 2012, promueve sus servicios de manera independiente, a través de <span>programas y proyectos en beneficio de la comunidad</span> con la cual ha trabajado durante años, especialmente la mujer emprendedora, líder, microempresaria y cabeza de hogar.
                    </p>
                    <br>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-9 text-center content-arrow">
                <a href="#cabezote-yaru"><i class="fa fa-angle-up" aria-hidden="true" style="margin-bottom: 2rem;"></i></a>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 vectorial text-center">
                <div class="col-md-8 col-lg-9">
                    <a href="http://vectorial.co/" target="_blank"><img src="<?php bloginfo('template_url') ?>/img/vectorial.png"></a>
                </div>
            </div>

        </div>


    </div>

    <div>
        <div class="col-md-4 col-lg-3 form-contacto">
            <h2><span>Déjanos tus datos</span></br>para contactarte</h2>
            <p>Para inscribirte en Yarú: Camino Integral para Emprendedoras, debes tener en cuenta los siguientes requisitos: </br></br>
                <span>- Ser mayor de 18 años</span></br>
                <span>- Tener un negocio</span></br>
                <span>- Vivir en estrato 1,2 o 3</span></br>
                <span>- Nivel educativo no superior al técnico</span></br>
            </p>
            <div class="form--container">
                <?php if ( have_posts() ) : ?>
                    <?php while ( have_posts() ) : the_post(); ?>

                        <?php echo the_content();?>

                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
<!--            <!-- Button trigger modal -->
<!--            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">-->
<!--                Launch demo modal-->
<!--            </button>-->

        </div>
        <div class="modal fade" id="error-formulario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i></button>
                    </div>
                    <div class="modal-body">

                        <h2>Hola, al parecer no has diligenciado los siguientes campos obligatorios del formulario:</h2>
                        <div class="contenido-mensaje" id="contenido-mensaje">
                            <ol>

                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- Button trigger modal -->

        <div class="col-xs-12 col-sm-12 col-md-12 vectorial2 text-center">
            <div class="col-md-8 col-lg-9">
                <a href="http://vectorial.co/" target="_blank"><img src="<?php bloginfo('template_url') ?>/img/vectorial.png"></a>
            </div>
        </div>
    </div>



<?php
wp_footer();
get_footer();
?>