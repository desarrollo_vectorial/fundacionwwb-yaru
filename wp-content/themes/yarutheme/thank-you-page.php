<?php
/* Template Name: Template Thanks Page*/
get_header();
wp_head();
?>

    <div id="fullpage">

        <!-- SECCION THANK YOU PAGE -->
        <div class="section cabezote-yaru-gracias">
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4 logo-yaru">
                            <img class="img-responsive" src="<?php bloginfo('template_url') ?>/img/logo-yaru-cabezote.png" alt="" width="100%">

                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8 title-cabezote">
                            <p class="text-left">Gracias por dejarnos tus datos y por tu interés en <span>Yarú: Camino Integral para Emprendedoras.</span>
                                </br>
                                Muy pronto nos pondremos en contacto contigo para continuar tu proceso.
                            </p>
                            <a href="https://www.fundacionwwbcolombia.org/yaru-camino-integral-emprendedoras/"> <div class="btn-volver">Volver a Yarú: Camino Integral para Emprendedoras</div></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- SECCION THANK YOU PAGE -->
    </div>
    <script>
        fbq('track', 'CompleteRegistration');
    </script>

<?php
wp_footer();
get_footer();
?>