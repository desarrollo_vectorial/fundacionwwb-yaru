$(document).ready(function() {
    $("input[type='submit']").click(function (){
        validateForm();
    });

    $('#error-formulario').on('hidden.bs.modal', function (e) {
        $("#contenido-mensaje > ol").empty();
    })
});
function validateForm(){

    var nombre = $('#nombre').val();
    var cedula = $('#cedula').val();
    var ciudadselected = $('.nice-select .list li.selected').data('value');
    var celular = $('#celular').val();

    var inputVal = new Array(nombre, cedula, ciudadselected, celular);

    var inputMessage = new Array(
        "Nombres y apellidos",
        "Cédula de ciudadanía",
        "Ciudad",
        "Teléfono celular"
    );
    // console.log(ciudadselected);
    //$('.error').hide();
    if(inputVal[3] == "" || inputVal[2]  == 'Ciudad *'  || inputVal[1] == "" || inputVal[0] == ""){

        if(inputVal[0] == ""){
            $('#contenido-mensaje ol').append('<li>' + inputMessage[0] + '</li>');
        }
        if(inputVal[1] == ""){
            $('#contenido-mensaje ol').append('<li>' + inputMessage[1] + '</li>');
        }
        if(inputVal[2] == 'Ciudad *'){
            $('#contenido-mensaje ol').append('<li>' + inputMessage[2] + '</li>');
        }
        if(inputVal[3] == ""){
            $('#contenido-mensaje ol').append('<li>' + inputMessage[3] + '</li>');
        }

        $('#error-formulario').modal('show');

    }

}