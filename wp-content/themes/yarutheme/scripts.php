<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="<?php bloginfo('template_url') ?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/jquery.fullPage.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/jquery.nice-select.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/validate-form.js"></script>
<script src="https://apps.elfsight.com/p/platform.js" defer></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#fullpage').fullpage({
            anchors: ['cabezote-yaru','que-es-yaru','como-lo-hacemos', 'sesiones','programa-fortaleser', 'elige-yaru', 'fundacionwwb'],
            sectionsColor: ['#fff','#fff','#fff','#f7f8fa','#fff', '#f0f1f2', '#f7f8fa'],
            navigation: true,
            resetSliders: true,
            navigationPosition: 'right',
            responsiveWidth: 992,
            afterResponsive: function(isResponsive){
            }

        });

        $('select').niceSelect();

        var click = 0;
        $("input[type='submit']").attr("id","btn-submit");
        console.log(click);

        $("select option:first-child").attr("disabled", "disabled");
        $(".nice-select li:first-child").attr("disabled", "disabled");


        document.addEventListener( 'wpcf7mailsent', function( event ) {
            if ( '5' == event.detail.contactFormId ) {
                //ga( 'send', 'event', 'Contact Form', 'submit' );
                location = 'https://www.fundacionwwbcolombia.org/yaru-camino-integral-emprendedoras/gracias';
            }
        }, false );


    });


</script>

