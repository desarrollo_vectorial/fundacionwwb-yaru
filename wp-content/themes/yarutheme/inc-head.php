<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <!--titulo-->
    <title><?php //bloginfo('name'); ?>Yarú: Camino Integral para Emprendedoras</title>
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/css/jquery.fullPage.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/css/main.css" />
    <!-- <link rel="stylesheet" type="text/css" href="<?php // bloginfo('template_url') ?>/css/nice-select.css" />-->
    <link href="<?php bloginfo('template_url') ?>/img/favicon-yaru.ico" rel="shortcut icon">
    <link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap
    <link href="css/bootstrap.css" rel="stylesheet"> -->
    <!--[if IE]> -->
</head>